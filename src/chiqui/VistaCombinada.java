/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chiqui;

import chiqui.ej01_singleton.vista.PanelEj01;
import chiqui.ej02_factoryMethod.vista.PanelEj02;
import chiqui.ej03_observer.vista.PanelEj03;
import chiqui.ej04_strategy.vista.PanelEj04;
import chiqui.ej05_decorator.vista.PanelEj05;
import java.awt.Font;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 *
 * @author thech
 */
public class VistaCombinada extends JFrame {

    private JTabbedPane tabs;

    public VistaCombinada(String title) throws HeadlessException {
        super(title);
        this.configurar();
    }

    private void configurar() {
        this.tabs = new JTabbedPane(JTabbedPane.TOP);
        this.tabs.setFont(new Font("Dialog", Font.BOLD, 16));
        this.tabs.addTab("Ej 01", new PanelEj01());
        this.tabs.addTab("Ej 02", new PanelEj02());
        this.tabs.addTab("Ej 03", new PanelEj03());
        this.tabs.addTab("Ej 04", new PanelEj04());
        this.tabs.addTab("Ej 05", new PanelEj05());
        this.add(this.tabs);

        this.pack();
        this.setLocationByPlatform(rootPaneCheckingEnabled);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setMinimumSize(this.getSize());
    }

}
