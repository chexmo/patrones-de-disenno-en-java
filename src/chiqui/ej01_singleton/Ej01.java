package chiqui.ej01_singleton;

import java.util.Objects;

public class Ej01 {

    private Chiqui elChiqui;

    public String crearChiqui() {
        if (Objects.isNull(this.elChiqui)) {
            this.elChiqui = Chiqui.getInstance();
            return "Nuevo Chiqui creado";
        } else {
            return "No se ha creado un nuevo Chiqui..." + "\n Chiqui hay uno solo.";
        }
    }

    public Chiqui getElChiqui() {
        return elChiqui;
    }

}
