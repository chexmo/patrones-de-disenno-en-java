package chiqui.ej01_singleton.vista;

import chiqui.ej01_singleton.Ej01;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelEj01 extends JPanel {

    private GroupLayout myGroupLayout;
    private JButton btCrearChiqui;
    private JLabel lbTexto;
    private JLabel lbTitulo;

    private Ej01 ej01 = new Ej01();

    public PanelEj01() {
        // Es importante que estos métodos se ejecuten en este orden
        this.configComponentes();
        this.maquetar();
    }

    private void configComponentes() {
        this.lbTitulo = new JLabel("Singleton");
        this.lbTitulo.setFont(
                new Font(
                        this.lbTitulo.getFont().getFontName(),
                        this.lbTitulo.getFont().getStyle(),
                        20
                )
        );

        btCrearChiqui = new JButton(" Crear un Chiqui ");
        btCrearChiqui.addActionListener(event -> this.crearChiqui());
        lbTexto = new JLabel("Naranja Fanta por ahora...");
    }

    private void maquetar() {
        myGroupLayout = new GroupLayout(this);
        myGroupLayout.setAutoCreateGaps(true);
        myGroupLayout.setAutoCreateContainerGaps(true);
        this.setLayout(myGroupLayout);

        myGroupLayout.setVerticalGroup(
                myGroupLayout.createSequentialGroup()
                        .addComponent(lbTitulo)
                        .addGap(20)
                        .addComponent(btCrearChiqui)
                        .addComponent(lbTexto)
        );

        myGroupLayout.setHorizontalGroup(
                myGroupLayout.createParallelGroup()
                        .addComponent(lbTitulo)
                        .addComponent(btCrearChiqui)
                        .addComponent(lbTexto)
        );
    }

    private void crearChiqui() {
        this.lbTexto.setText(this.ej01.crearChiqui());
    }

}
