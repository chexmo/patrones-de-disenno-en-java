package chiqui.ej01_singleton;

public class Chiqui {

    private static Chiqui instancia;

    protected String nombre;
    private String apellido;

    private Chiqui() {
    }

    private Chiqui(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public static Chiqui getInstance() {
        if (instancia == null) {
            instancia = new Chiqui();
            return instancia;
        } else {
            return instancia;
        }
    }

    public void setNombre(String n) {
        this.nombre = n;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

}
