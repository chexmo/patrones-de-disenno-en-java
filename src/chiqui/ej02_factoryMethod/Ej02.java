package chiqui.ej02_factoryMethod;

import chiqui.ej02_factoryMethod.programadores.Programador;
import chiqui.ej02_factoryMethod.programas.Programa;
import java.util.ArrayList;

public class Ej02 {

    private ArrayList<Programa> programasCreados = new ArrayList<>();

    public ArrayList<Programa> getProgramasCreados() {
        return programasCreados;
    }

    public void crearPrograma(String tipo, String nombre) throws Exception {
        Programa.LENGUAJE leng = Programa.lenguajeStrToEnum(tipo);
        Programa programa = Programador.programar(nombre, leng);
        this.programasCreados.add(programa);
    }
}
