package chiqui.ej02_factoryMethod.vista;

import chiqui.ej02_factoryMethod.Ej02;
import chiqui.ej02_factoryMethod.programas.Programa;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class PanelEj02 extends JPanel {

    private Ej02 ej02 = new Ej02();

    private JLabel lbTitulo;
    private GroupLayout layout;
    private JLabel lbTipo;
    private JComboBox<Programa.LENGUAJE> cbTipo;
    private JLabel lbNombre;
    private JTextField tfNombre;
    private JTable tbTabla;
    private JScrollPane spDeslizable;
    private JButton btCrear;

    public PanelEj02() {
        this.configComponentes();
        this.maquetar();
    }

    private void configComponentes() {
        this.layout = new GroupLayout(this);
        this.layout.setAutoCreateContainerGaps(true);
        this.layout.setAutoCreateGaps(true);
        this.setLayout(this.layout);

        this.lbTitulo = new JLabel("Factory method");
        this.lbTitulo.setFont(
                new Font(
                        this.lbTitulo.getFont().getFontName(),
                        this.lbTitulo.getFont().getStyle(),
                        20
                )
        );

        this.lbTipo = new JLabel("Seleccione un lenguaje");
        this.cbTipo = new JComboBox();
        this.cbTipo.setMaximumSize(new Dimension(200, 25));
        this.cbTipo.setModel(
                new DefaultComboBoxModel(
                        (Arrays.asList(Programa.LENGUAJE.values()))
                                .stream()
                                .map(l -> l.toString())
                                .collect(Collectors.toList())
                                .toArray()
                )
        );

        this.lbNombre = new JLabel("Nombre del programa");
        this.tfNombre = new JTextField("");
        this.tfNombre.setMaximumSize(new Dimension(200, 25));
        this.tfNombre.setMinimumSize(new Dimension(200, 25));
        this.tfNombre.addActionListener(e -> crearPrograma());

        this.btCrear = new JButton("Fabricar");
        this.btCrear.setMaximumSize(new Dimension(100, 20));
        this.btCrear.addActionListener(e -> crearPrograma());

        this.tbTabla = new JTable();
        this.tbTabla.setModel(this.tableModelProgramasCreados());
        this.spDeslizable = new JScrollPane(tbTabla);
        this.spDeslizable.setMaximumSize(new Dimension(500, 200));

    }

    private void maquetar() {
        this.layout.setVerticalGroup(this.layout.createSequentialGroup()
                .addComponent(lbTitulo)
                .addGap(20)
                .addGroup(this.layout.createParallelGroup()
                        .addComponent(lbTipo)
                        .addComponent(cbTipo)
                )
                .addGroup(this.layout.createParallelGroup()
                        .addComponent(lbNombre)
                        .addComponent(tfNombre)
                        .addComponent(btCrear)
                )
                .addComponent(spDeslizable)
        );

        this.layout.setHorizontalGroup(
                this.layout.createParallelGroup()
                        .addGroup(this.layout.createSequentialGroup()
                                .addGroup(this.layout.createParallelGroup()
                                        .addComponent(lbTitulo)
                                        .addComponent(lbTipo)
                                        .addComponent(lbNombre)
                                )
                                .addGroup(this.layout.createParallelGroup()
                                        .addComponent(cbTipo)
                                        .addComponent(tfNombre)
                                )
                                .addGroup(this.layout.createParallelGroup()
                                        .addComponent(btCrear)
                                )
                        )
                        .addComponent(spDeslizable)
        );

    }

    public DefaultTableModel tableModelProgramasCreados() {
        ArrayList<Programa> prgs = ej02.getProgramasCreados();
        int cantProgramas = prgs.size();
        String[] columnas = new String[]{"Programa", "Descripcion", "Lenguaje"};
        String[][] contenido = new String[cantProgramas][columnas.length];

        for (int i = 0; i < cantProgramas; i++) {
            contenido[i][0] = prgs.get(i).getNombre();
            contenido[i][1] = prgs.get(i).getDescripcion();
            contenido[i][2] = prgs.get(i).getLeng().toString();
        }

        DefaultTableModel dtm = new DefaultTableModel(contenido, columnas) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return dtm;
    }

    void crearPrograma() {
        String tipoSeleccionado = (String) this.cbTipo.getSelectedItem();
        String nombreSeleccionado = this.tfNombre.getText();
        try {
            this.ej02.crearPrograma(tipoSeleccionado, nombreSeleccionado);
        } catch (Exception ex) {
            new JOptionPane("ERROR AL INTENTAR CREAR UN PROGRAMA", JOptionPane.ERROR_MESSAGE)
                    .setVisible(true);
        }
        this.tbTabla.setModel(tableModelProgramasCreados());
    }
}
