package chiqui.ej02_factoryMethod.programadores;

import chiqui.ej02_factoryMethod.programas.Programa;
import chiqui.ej02_factoryMethod.programas.ProgramaPython;

public class ProgramadorPython implements Programador {

    @Override
    public Programa crearPrograma(String titulo) {
        System.out.println("Programando "
                + titulo
                + " en python");
        return new ProgramaPython(titulo, "Sin descripcion");
    }
}
