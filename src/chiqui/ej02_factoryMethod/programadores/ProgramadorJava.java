package chiqui.ej02_factoryMethod.programadores;

import chiqui.ej02_factoryMethod.programas.Programa;
import chiqui.ej02_factoryMethod.programas.ProgramaJava;

public class ProgramadorJava implements Programador {

    @Override
    public Programa crearPrograma(String titulo) {
        System.out.println("Programando "
                + titulo
                + " en java");
        return new ProgramaJava(titulo, "Sin descripcion");
    }
}
