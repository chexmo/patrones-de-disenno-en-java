package chiqui.ej02_factoryMethod.programadores;

import chiqui.ej02_factoryMethod.programas.Programa;
import static chiqui.ej02_factoryMethod.programas.Programa.LENGUAJE;

public interface Programador {

    
    Programa crearPrograma(String titulo);
    

    // Factory Method
    public static Programa programar(String titulo, LENGUAJE lenguaje) throws Exception {
        Programador prog;
        switch (lenguaje) {
            case JAVA:
                prog = new ProgramadorJava();
                break;
            case PYTHON:
                prog = new ProgramadorPython();
                break;
            default:
                throw new Exception("Tipo de programador inválido");
        }
        return prog.crearPrograma(titulo);
    }
}
