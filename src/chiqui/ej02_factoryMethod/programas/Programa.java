package chiqui.ej02_factoryMethod.programas;

public interface Programa {

    public enum LENGUAJE {
        JAVA, PYTHON
    };

    public String getDescripcion();

    public String getNombre();

    public LENGUAJE getLeng();
    
    public static LENGUAJE lenguajeStrToEnum(String lenguaje) {
        lenguaje = lenguaje.toLowerCase();
        switch (lenguaje) {
            case "java":
                return LENGUAJE.JAVA;
            case "python":
                return LENGUAJE.PYTHON;
            default:
                return null;
        }
    }
}
