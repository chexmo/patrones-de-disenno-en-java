package chiqui.ej02_factoryMethod.programas;

public class ProgramaPython implements Programa {

    private String nombre;
    private String descripcion;
    private String pythonVersion;
    private static String currentVersion = "Python 3";

    public ProgramaPython(String nombre, String descripcion) {
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.pythonVersion = currentVersion;
    }

    @Override
    public String getDescripcion() {
        return this.descripcion;
    }

    @Override
    public String getNombre() {
        return this.nombre;
    }

    @Override
    public LENGUAJE getLeng() {
        return LENGUAJE.PYTHON;
    }
}
