package chiqui.ej02_factoryMethod.programas;

public class ProgramaJava implements Programa {

    private String descripcion;
    private String nombre;
    private String javaVersion;
    private static String currentVersion = "Java 8";

    public ProgramaJava(String nombre, String descripcion) {
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.javaVersion = currentVersion;
    }

    @Override
    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    @Override
    public LENGUAJE getLeng() {
        return LENGUAJE.JAVA;
    }
}
