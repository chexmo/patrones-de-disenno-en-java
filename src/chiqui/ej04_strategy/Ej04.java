package chiqui.ej04_strategy;

import chiqui.ej04_strategy.estrategias.Defensivo;
import chiqui.ej04_strategy.estrategias.FuegoADiscresion;
import chiqui.ej04_strategy.estrategias.FuegoAbierto;
import chiqui.ej04_strategy.estrategias.Neutral;
import chiqui.ej04_strategy.estrategias.SinEstrategia;
import java.util.ArrayList;

public class Ej04 {

    private static final ArrayList<Estrategia> estrategias = new ArrayList<>();

    private Tropa tropa = new Tropa(estrategias.get(0));

    static { //Similar a _ready()
        estrategias.add(new SinEstrategia());
        estrategias.add(new Defensivo());
        estrategias.add(new FuegoADiscresion());
        estrategias.add(new FuegoAbierto());
        estrategias.add(new Neutral());
    }

    public void cambiarEstrategia(String nombreEstrategia) {
        Estrategia est = buscarEstrategia(nombreEstrategia);
        this.tropa.setEstrategia(est);
    }

    public String getEstrategiaActual() {
        return this.tropa.getEstrategia().getClass().getSimpleName();
    }

    public String[] getEstrategiasAsStrings() {
        String[] response = new String[estrategias.size()];
        for (int i = 0; i < estrategias.size(); i++) {
            response[i] = estrategias.get(i).getClass().getSimpleName();
        }
        return response;
    }

    private Estrategia buscarEstrategia(String nombreEstrategia) {
        for (Estrategia e : estrategias) {
            if (e.getClass().getSimpleName().equals(nombreEstrategia)) {
                return e;
            }
        }
        return estrategias.get(0);
    }

    public String atacar() {
        return tropa.atacar();
    }

    public String defender() {
        return tropa.defender();
    }

    public String situar() {
        return tropa.situar();
    }
}
