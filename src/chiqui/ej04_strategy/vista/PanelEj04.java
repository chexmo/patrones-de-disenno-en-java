package chiqui.ej04_strategy.vista;

import chiqui.ej04_strategy.Ej04;
import java.awt.Color;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

public class PanelEj04 extends JPanel {

    private Ej04 ej04 = new Ej04();

    private GroupLayout layout;

    private JLabel lbTitulo;
    private JButton btAtacar;
    private JButton btDefender;
    private JButton btSituar;
    private JLabel lbResultado;
    private JList<String> lsEstrategias;

    public PanelEj04() {
        this.configComponentes();
        this.maquetar();
    }

    private void configComponentes() {
        this.layout = new GroupLayout(this);
        this.layout.setAutoCreateContainerGaps(true);
        this.layout.setAutoCreateGaps(true);
        this.setLayout(this.layout);

        this.lbTitulo = new JLabel("Strategy");
        this.lbTitulo.setFont(
                new Font(
                        this.lbTitulo.getFont().getFontName(),
                        this.lbTitulo.getFont().getStyle(),
                        20
                )
        );

        this.btAtacar = new JButton(" Atacar ");
        this.btAtacar.addActionListener(e -> this.atacar());

        this.btDefender = new JButton(" Defender ");
        this.btDefender.addActionListener(e -> this.defender());

        this.btSituar = new JButton(" Situar ");
        this.btSituar.addActionListener(e -> this.situar());

        this.lbResultado = new JLabel(" ");
        this.lbResultado.setForeground(Color.black);
        this.lbResultado.setFont(
                new Font(
                        this.lbResultado.getFont().getFontName(),
                        this.lbResultado.getFont().getStyle(),
                        15
                )
        );

        this.lsEstrategias = new JList<>(this.ej04.getEstrategiasAsStrings());
        this.lsEstrategias.addListSelectionListener(e -> this.seleccionar());

    }

    private void maquetar() {
        this.layout.setVerticalGroup(
                this.layout.createSequentialGroup()
                        .addComponent(lbTitulo)
                        .addGap(20)
                        .addGroup(this.layout.createParallelGroup()
                                .addGroup(this.layout.createSequentialGroup()
                                        .addComponent(lsEstrategias)
                                )
                                .addGroup(this.layout.createSequentialGroup()
                                        .addComponent(btAtacar)
                                        .addComponent(btDefender)
                                        .addComponent(btSituar)
                                )
                        )
                        .addComponent(lbResultado)
        );

        this.layout.setHorizontalGroup(this.layout.createParallelGroup()
                .addComponent(lbTitulo)
                .addGroup(this.layout.createSequentialGroup()
                        .addGroup(this.layout.createParallelGroup()
                                .addComponent(lsEstrategias)
                        )
                        .addGap(30)
                        .addGroup(this.layout.createParallelGroup()
                                .addComponent(btAtacar)
                                .addComponent(btDefender)
                                .addComponent(btSituar)
                        )
                )
                .addComponent(lbResultado)
        );

    }

    private void atacar() {
        lbResultado.setText(ej04.atacar());
        lbResultado.setForeground(Color.red);
    }

    private void defender() {
        lbResultado.setText(ej04.defender());
        lbResultado.setForeground(Color.blue);
    }

    private void situar() {
        lbResultado.setText(ej04.situar());
        lbResultado.setForeground(Color.green);
    }

    private void seleccionar() {
        ej04.cambiarEstrategia(lsEstrategias.getSelectedValue());
    }
}
