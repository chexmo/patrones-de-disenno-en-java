package chiqui.ej04_strategy.estrategias;

import chiqui.ej04_strategy.Estrategia;

public class FuegoAbierto implements Estrategia {

    @Override
    public String atacar() {
        return "Si algo se mueve, disparale por las dudas.";
    }

    @Override
    public String defender() {
        return "No hay mejor defense que un sólido ataque.";
    }

    @Override
    public String situar() {
        return "Dinamico. Se avanza hacia el enemigo.";
    }

}
