package chiqui.ej04_strategy.estrategias;

import chiqui.ej04_strategy.Estrategia;

public class FuegoADiscresion implements Estrategia {

    @Override
    public String atacar() {
        return "Atacando a los enemigos de un cierto rango";
    }

    @Override
    public String defender() {
        return "Defensa activa.";
    }

    @Override
    public String situar() {
        return "A cubierto pero con vision favorable.";
    }

}
