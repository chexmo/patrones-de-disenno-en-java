package chiqui.ej04_strategy.estrategias;

import chiqui.ej04_strategy.Estrategia;

public class Defensivo implements Estrategia {

    @Override
    public String atacar() {
        return "Manso como un cordero";
    }

    @Override
    public String defender() {
        return "¿Te enteraste lo que le paso a Perl Harbor?";
    }

    @Override
    public String situar() {
        return "Buscar puestos estratégicos";
    }

}
