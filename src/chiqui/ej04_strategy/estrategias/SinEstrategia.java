package chiqui.ej04_strategy.estrategias;

import chiqui.ej04_strategy.Estrategia;

public class SinEstrategia implements Estrategia {

    @Override
    public String atacar() {
        return "Atacar???";
    }

    @Override
    public String defender() {
        return "Defender???";
    }

    @Override
    public String situar() {
        return "Situar???";
    }

}
