package chiqui.ej04_strategy.estrategias;

import chiqui.ej04_strategy.Estrategia;

public class Neutral implements Estrategia {

    @Override
    public String atacar() {
        return "Si no me jodes yo no te jodo.";
    }

    @Override
    public String defender() {
        return "No te atrevas a tocarnos";
    }

    @Override
    public String situar() {
        return "Me quedo aca y punto";
    }

}
