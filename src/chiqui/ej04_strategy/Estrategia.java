package chiqui.ej04_strategy;

public interface Estrategia {

    public String atacar();

    public String defender();

    public String situar();

}
