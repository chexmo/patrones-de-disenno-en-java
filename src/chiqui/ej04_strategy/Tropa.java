package chiqui.ej04_strategy;

public class Tropa {

    private Estrategia estrategia;

    public Tropa(Estrategia estrategia) {
        this.estrategia = estrategia;
    }

    public Estrategia getEstrategia() {
        return estrategia;
    }

    public void setEstrategia(Estrategia estrategia) {
        this.estrategia = estrategia;
    }

    public String atacar() {
        return estrategia.atacar();
    }

    public String defender() {
        return estrategia.defender();
    }

    public String situar() {
        return estrategia.situar();
    }

}
