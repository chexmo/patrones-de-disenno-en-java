package chiqui.ej03_observer;

import chiqui.ej03_observer.aberturas.Puerta;
import chiqui.ej03_observer.aberturas.Ventana;
import chiqui.ej03_observer.actores.Celular;
import chiqui.ej03_observer.actores.Policia;
import chiqui.ej03_observer.actores.Vecino;
import chiqui.ej03_observer.alarma.AlarmaDomiciliaria;
import java.util.ArrayList;

public class Ej03 {

    private AlarmaDomiciliaria alarma;
    private ArrayList<Abertura> misAberturas = new ArrayList<>();
    private ArrayList<Actor> misActores = new ArrayList<>();

    public Ej03(Observador observador) {
        this.crearAlgunasAberturas();
        this.crearAlgunosActores();
        this.alarma = new AlarmaDomiciliaria(misAberturas, misActores);

        this.alarma.registrarObservador(observador);
        // Ni Messi se animó a gambetear así la falencia de un observador en esta clase

    }

    private void crearAlgunasAberturas() {
        this.misAberturas.add(new Puerta("Puerta frontal"));
        this.misAberturas.add(new Puerta("Puerta del patio"));
        this.misAberturas.add(new Ventana("Ventana frontal"));
        this.misAberturas.add(new Ventana("Ventana de habitacion"));
        this.misAberturas.add(new Ventana("Ventana del patio"));
    }

    private void crearAlgunosActores() {
        this.misActores.add(new Policia("Policia Area 51"));
        this.misActores.add(new Vecino("Vecino de al lado"));
        this.misActores.add(new Celular("Celular del dueño"));
    }

    public AlarmaDomiciliaria getAlarma() {
        return alarma;
    }

    public ArrayList<String> getNombresActores() {
        ArrayList<String> nom = new ArrayList<>();
        this.alarma
                .getObservadores()
                .stream()
                .filter(o -> (o instanceof Actor))
                .map(o -> (Actor) o)
                .map(a -> a.getNombre())
                .forEach(n -> nom.add(n));
        return nom;
    }

}
