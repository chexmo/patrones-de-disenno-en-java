package chiqui.ej03_observer;

public interface Abertura {

    public void setCerrada(boolean cerrada);

    public boolean isCerrada();
    
    public String getDescripcion();

}
