package chiqui.ej03_observer.aberturas;

import java.util.ArrayList;
import jdk.nashorn.internal.runtime.arrays.ArrayLikeIterator;
import chiqui.ej03_observer.Abertura;
import chiqui.ej03_observer.Observable;
import chiqui.ej03_observer.Observador;

public class Puerta implements Abertura, Observable {

    private ArrayList<Observador> observadores;
    private boolean cerrada = false;
    private String descripcion;

    public Puerta(String descripcion) {
        this.observadores = new ArrayList<>();
        this.descripcion = descripcion;
    }

    @Override
    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public void setCerrada(boolean cerrada) {
        System.out.println("PUERTA: setCerrada("+cerrada+")");
        this.cerrada = cerrada;
        this.notificarObservadores();
    }

    @Override
    public boolean isCerrada() {
        return this.cerrada;
    }

    @Override
    public void registrarObservador(Observador observador) {
        this.observadores.add(observador);
    }

    @Override
    public void removerObservador(Observador observador) {
        this.observadores.remove(observador);
    }

    @Override
    public void notificarObservadores() {
        for (Observador o : this.observadores) {
            o.actualizar(this);
        }
    }

}
