package chiqui.ej03_observer.vista;

import chiqui.ej03_observer.Ej03;
import chiqui.ej03_observer.Observable;
import chiqui.ej03_observer.Observador;
import chiqui.ej03_observer.alarma.AlarmaDomiciliaria;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import static javax.swing.SwingConstants.LEFT;

public class PanelEj03 extends JPanel implements Observador {

    private Ej03 ej03 = new Ej03(this);

    private JPanel panelAberturas;
    private JPanel panelActores;
    private JPanel panelAlarma;
    private ImageIcon iconOff;
    private ImageIcon iconOn;
    private ImageIcon iconAlert;
    private JLabel estadoAlarma;
    private GroupLayout layout;
    private JLabel lbTitulo;

    public PanelEj03() {
        // Es MUY necesario que estas funciones
        // se ejecuten en este orden #SmellsLikeATemplate
        this.loadIcons();
        this.configComponentes();
        this.maquetar();
    }

    private void loadIcons() {
        // Indicador del estado de la alarma
        try {
            this.iconOff = new ImageIcon(
                    ImageIO
                            .read(new File("icons/off.png"))
                            .getScaledInstance(150, 150, Image.SCALE_SMOOTH)
            );

            this.iconOn = new ImageIcon(
                    ImageIO
                            .read(new File("icons/ok.png"))
                            .getScaledInstance(150, 150, Image.SCALE_SMOOTH)
            );

            this.iconAlert = new ImageIcon(
                    ImageIO
                            .read(new File("icons/cruz.png"))
                            .getScaledInstance(150, 150, Image.SCALE_SMOOTH)
            );

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(
                    this,
                    "Error al cargar las imágenes del ejercicio 3",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private void configComponentes() {
        this.layout = new GroupLayout(this);
        this.layout.setAutoCreateContainerGaps(true);
        this.layout.setAutoCreateGaps(true);
        this.setLayout(this.layout);

        this.lbTitulo = new JLabel("Observer");
        this.lbTitulo.setFont(
                new Font(
                        this.lbTitulo.getFont().getFontName(),
                        this.lbTitulo.getFont().getStyle(),
                        20
                )
        );

        this.panelAberturas = crearPanelAberturas();
        this.panelActores = crearPanelActores();
        this.panelAlarma = crearPanelAlarma();
    }

    private void maquetar() {

        this.layout.setVerticalGroup(this.layout.createSequentialGroup()
                .addComponent(this.lbTitulo)
                .addGap(20)
                .addGroup(this.layout.createParallelGroup()
                        .addComponent(this.panelAberturas)
                        .addComponent(this.panelAlarma)
                        .addComponent(this.panelActores)
                )
        );

        this.layout.setHorizontalGroup(this.layout.createParallelGroup()
                .addComponent(this.lbTitulo)
                .addGroup(this.layout.createSequentialGroup()
                        .addComponent(this.panelAberturas)
                        .addComponent(this.panelAlarma)
                        .addComponent(this.panelActores)
                )
        );

    }

    private JPanel crearPanelAberturas() {
        JPanel panel = new JPanel();
        BoxLayout bl = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(bl);

        this.ej03
                .getAlarma()
                .getAberturas()
                .stream()
                .map(
                        a -> {
                            JCheckBox jcb = new JCheckBox(a.getDescripcion());
                            jcb.addActionListener(e -> a.setCerrada(jcb.isSelected()));
                            return jcb;
                        }
                )
                .forEachOrdered(jcb -> panel.add(jcb));
        return panel;
    }

    private JPanel crearPanelActores() {
        JPanel panel = new JPanel();
        BoxLayout bl2 = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(bl2);

        this.ej03.getNombresActores()
                .stream()
                .forEach(
                        n -> {
                            JCheckBox jcb = new JCheckBox(n);
                            jcb.setAlignmentX(RIGHT_ALIGNMENT);
                            jcb.setHorizontalTextPosition(LEFT);
                            jcb.setEnabled(false);
                            panel.add(jcb);
                        }
                );

        return panel;
    }

    private JPanel crearPanelAlarma() {
        JPanel panel = new JPanel();
        BoxLayout bl3 = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(bl3);

        JToggleButton jtb = new JToggleButton("ACTIVAR ALARMA");
        jtb.addActionListener(
                e -> {
                    this.ej03.getAlarma().setActivada(jtb.isSelected());
                    if (this.ej03.getAlarma().isActivada()) {
                        this.estadoAlarma.setIcon(this.iconOn);
                        jtb.setText("DESACTIVAR ALARMA");
                    } else {
                        this.estadoAlarma.setIcon(this.iconOff);
                        this.setAllActoresJCheckBoxesTo(false);
                        jtb.setText("ACTIVAR ALARMA");
                    }
                }
        );
        jtb.setAlignmentX(CENTER_ALIGNMENT);
        panel.add(jtb);

        this.estadoAlarma = new JLabel(this.iconOff);
        this.estadoAlarma.setAlignmentX(CENTER_ALIGNMENT);
        panel.add(this.estadoAlarma);

        return panel;
    }

    private void setAllActoresJCheckBoxesTo(boolean state) {
        Stream.of(this.panelActores.getComponents())
                .filter(c -> c instanceof JCheckBox)
                .map(c -> (JCheckBox) c)
                .forEach(c -> c.setSelected(state));
    }

    @Override
    public void actualizar(Observable o) {
        if (o.equals(this.ej03.getAlarma())) {
            AlarmaDomiciliaria a = (AlarmaDomiciliaria) o;
            if (a.isSonando()) {
                this.estadoAlarma.setIcon(this.iconAlert);
                this.setAllActoresJCheckBoxesTo(true);
            }
        }
    }
}
