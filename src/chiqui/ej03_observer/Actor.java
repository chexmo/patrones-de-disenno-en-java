package chiqui.ej03_observer;

import chiqui.ej03_observer.alarma.AlarmaDomiciliaria;

public class Actor implements Observador {

    protected boolean enAlerta;
    protected String nombre;

    public Actor(String nombre) {
        this.nombre = nombre;
        this.enAlerta = false;
    }

    public boolean isEnAlerta() {
        return enAlerta;
    }

    public void setEnAlerta(boolean enAlerta) {
        this.enAlerta = enAlerta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void actualizar(Observable o) {
        if (o instanceof AlarmaDomiciliaria) {
            this.setEnAlerta(true);
        }
    }

}
