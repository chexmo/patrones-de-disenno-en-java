package chiqui.ej03_observer;

public interface Observador {
    
    public void actualizar(Observable o);
    
}
