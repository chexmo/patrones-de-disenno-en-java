package chiqui.ej03_observer.alarma;

import chiqui.ej03_observer.Abertura;
import chiqui.ej03_observer.Actor;
import chiqui.ej03_observer.Observable;
import chiqui.ej03_observer.Observador;
import java.util.ArrayList;

public class AlarmaDomiciliaria implements Observable, Observador {

    private ArrayList<Observador> observadores = new ArrayList<>();
    private ArrayList<Abertura> aberturas = new ArrayList<>();
    private boolean activada = false;
    private boolean sonando = false;

    public AlarmaDomiciliaria(ArrayList<Abertura> misAberturas, ArrayList<Actor> misActores) {
        this.registrarAberturas(misAberturas);
        this.observadores = new ArrayList<>(misActores);
    }

    private void registrarAberturas(ArrayList<Abertura> misAberturas) {
        misAberturas
                .stream()
                .filter(a -> !(this.aberturas.contains(a)))
                .forEach(a -> this.aberturas.add(a));
        this.aberturas
                .stream()
                .filter(a -> (a instanceof Observable))
                .map(a -> (Observable) a)
                .forEach(o -> o.registrarObservador(this));
    }

    public boolean isActivada() {
        return activada;
    }

    public void setActivada(boolean activada) {
        System.out.println("ALARMA: setActivada(" + activada + ")");
        if (activada == false) {
            this.setSonando(false);
        }
        this.activada = activada;
    }

    public ArrayList<Observador> getObservadores() {
        return observadores;
    }

    public void setObservadores(ArrayList<Observador> observadores) {
        this.observadores = observadores;
    }

    public ArrayList<Abertura> getAberturas() {
        return aberturas;
    }

    public void setAberturas(ArrayList<Abertura> aberturas) {
        this.aberturas = aberturas;
    }

    public boolean isSonando() {
        return sonando;
    }

    public void setSonando(boolean sonando) {
        System.out.println("ALARMA: setSonando(" + sonando + ")");
        this.sonando = sonando;
        this.notificarObservadores();
    }

    @Override
    public void registrarObservador(Observador observador) {
        if (!this.observadores.contains(observador)) {
            this.observadores.add(observador);
        }
    }

    @Override
    public void removerObservador(Observador observador) {
        this.observadores.remove(observador);
    }

    @Override
    public void notificarObservadores() {
        for (Observador o : this.observadores) {
            o.actualizar(this);
        }
    }

    @Override
    public void actualizar(Observable o) {
        if (o instanceof Abertura) {
            if (activada == true) {
                this.setSonando(true);
            }
        }
    }

}
