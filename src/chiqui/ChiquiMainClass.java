package chiqui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class ChiquiMainClass {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ChiquiMainClass.class.getName()).log(Level.SEVERE, null, ex);
        }

        VistaCombinada vc = new VistaCombinada("TP de Design Patterns");
        vc.setVisible(true);

        //TODO EJ05 = Decorator
        //TODO EJ06 = Composite
    }
}
