package chiqui.ej05_decorator.accesorios;

import chiqui.ej05_decorator.Accesorio;
import chiqui.ej05_decorator.Vendible;
import java.util.List;

/**
 *
 * @author Chiqui Chexmo <thechexmo@gmail.com>
 */
public class Luces extends Accesorio {

    public Luces(float precio, Vendible contenido) {
        super(precio, contenido);
    }

    @Override
    public List<String> getDescripciones() {
        List<String> descripciones = contenido.getDescripciones();
        descripciones.add("Luces que se prenden, se apagan, se prenden, etc.");
        return descripciones;
    }

}
