package chiqui.ej05_decorator.accesorios;

import chiqui.ej05_decorator.Accesorio;
import chiqui.ej05_decorator.Vendible;
import java.util.List;

/**
 *
 * @author Chiqui Chexmo <thechexmo@gmail.com>
 */
public class Estrellas extends Accesorio {

    public Estrellas(int precio, Vendible contenido) {
        super(precio, contenido);
    }

    @Override
    public List<String> getDescripciones() {
        List<String> descripciones = contenido.getDescripciones();
        descripciones.add("Estrellas recién caídas del cielo");
        return descripciones;
    }

}
