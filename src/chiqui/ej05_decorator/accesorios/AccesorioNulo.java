package chiqui.ej05_decorator.accesorios;

import chiqui.ej05_decorator.Accesorio;
import chiqui.ej05_decorator.Vendible;
import java.util.List;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class AccesorioNulo extends Accesorio {

    public AccesorioNulo(float precio, Vendible contenido) {
        super(0f, contenido);
    }

    @Override
    public List<String> getDescripciones() {
        return contenido.getDescripciones();
    }

}
