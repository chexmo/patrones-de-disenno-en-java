package chiqui.ej05_decorator.vista;

import chiqui.ej05_decorator.Ej05;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author Chiqui Chexmo -- thechexmo@gmail.com
 */
public class PanelEj05 extends JPanel {

    private Ej05 ej05 = new Ej05();
    private JLabel lbTitulo;
    private JLabel lbAgregar;
    private JLabel lbIntro;
    private JPanel pnPrevisualizacion;
    private JLabel lbTotal;
    private GroupLayout lm;
    private JLabel lbTxtPrevi;
    private JComboBox<Object> cbArbolito;
    private JComboBox<Object> cbAccDisp;

    public PanelEj05() {
        this.iniciar();
    }

    private void iniciar() {
        this.lm = new GroupLayout(this);
        this.setLayout(lm);

        this.lbTitulo = new JLabel("Decorator");
        this.lbTitulo.setFont(
                new Font(
                        this.lbTitulo.getFont().getFontName(),
                        this.lbTitulo.getFont().getStyle(),
                        20
                )
        );

        this.lbIntro = new JLabel("Programa para armar y vender arbolitos");
        this.cbArbolito = new JComboBox<>(ej05.getTamannosArbol().toArray(new String[0]));
        this.cbArbolito.addActionListener(this::nuevoArbolito);

        this.lbAgregar = new JLabel("Agregar adorno: ");
        this.cbAccDisp = new JComboBox<>(ej05.getAccesorios().toArray(new String[0]));
        this.cbAccDisp.addActionListener(this::accesorioSeleccionado);

        this.pnPrevisualizacion = new JPanel();
        this.pnPrevisualizacion.setAlignmentX(CENTER_ALIGNMENT);
        this.pnPrevisualizacion.setAlignmentY(CENTER_ALIGNMENT);
        this.pnPrevisualizacion.setBackground(Color.WHITE);
        this.pnPrevisualizacion.add(new JLabel("......."));

        this.lbTotal = new JLabel(this.crearTextoParaTotal(0f));
        this.lbTotal.setFont(
                new Font(
                        this.lbTitulo.getFont().getFontName(),
                        this.lbTitulo.getFont().getStyle(),
                        20
                )
        );

        lm.setHorizontalGroup(lm.createParallelGroup()
                .addComponent(lbTitulo)
                .addGap(20)
                .addGroup(lm.createSequentialGroup()
                        .addComponent(lbIntro)
                        .addGap(20)
                        .addComponent(cbArbolito)
                )
                .addGroup(lm.createSequentialGroup()
                        .addComponent(lbAgregar)
                        .addGap(20)
                        .addComponent(cbAccDisp)
                )
                .addComponent(pnPrevisualizacion, GroupLayout.Alignment.CENTER)
                .addComponent(lbTotal, GroupLayout.Alignment.TRAILING)
        );

        lm.setVerticalGroup(lm.createSequentialGroup()
                .addComponent(lbTitulo)
                .addGap(20)
                .addGroup(lm.createBaselineGroup(true, true)
                        .addComponent(lbIntro)
                        .addComponent(cbArbolito)
                )
                .addGap(20)
                .addGroup(lm.createBaselineGroup(true, true)
                        .addComponent(lbAgregar)
                        .addComponent(cbAccDisp, 20, 25, 30)
                )
                .addComponent(pnPrevisualizacion, 50, 200, 500)
                .addComponent(lbTotal)
        );
    }

    private String crearTextoParaTotal(float valor) {
        return String.format("Total: $%.2f", valor);
    }

    private void setTotalEnLabel(float valor) {
        this.lbTotal.setText(crearTextoParaTotal(valor));
    }

    private void accesorioSeleccionado(ActionEvent e) {
        String seleccionado = null;
        try {
            JComboBox elem = (JComboBox) e.getSource();
            seleccionado = (String) elem.getSelectedItem();
            System.out.println("Se ha seleccionado: " + seleccionado);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this,
                    "Error al seleccionar un accesorio",
                    "ERROR INESPERADO",
                    JOptionPane.ERROR_MESSAGE);
        }
        if (seleccionado != null && !seleccionado.equals("")) {
            ej05.agregarAccesorio(seleccionado);
            this.previsualizar();
        }
    }

    private void nuevoArbolito(ActionEvent e) {
        ej05.nuevoArbol((String) cbArbolito.getSelectedItem());
        setTotalEnLabel(ej05.getPedido().getPrecioTotal());
        previsualizar();
    }

    private void previsualizar() {
        List<String> descripciones = ej05.getPedido().getDescripciones();

        this.pnPrevisualizacion.removeAll();
        this.lbTxtPrevi = new JLabel(descripciones.get(0));
        this.pnPrevisualizacion.add(lbTxtPrevi);

        LinkedList<String> lst = new LinkedList<>(descripciones);
        lst.removeFirst();
        this.agregarBordes(lst);

        this.redibujarPrevisualizacion();
    }

    private void redibujarPrevisualizacion() {
        try {
            Thread.sleep(200); //Para prevenir una "racing condition"
        } catch (InterruptedException ex) {
        }
        this.validate();
    }

    private void agregarBordes(LinkedList<String> lst) {
        if (lst.size() > 0) {
            Border bViejo = this.lbTxtPrevi.getBorder();
            Border bNuevo = null;

            if (bViejo == null) {
                bNuevo = BorderFactory.createTitledBorder(lst.removeFirst());
            } else {
                Border bAgregando = BorderFactory.createTitledBorder(lst.removeFirst());
                bNuevo = BorderFactory.createCompoundBorder(bAgregando, bViejo);
            }

            this.lbTxtPrevi.setBorder(bNuevo);
            agregarBordes(lst);
        }
    }
}
