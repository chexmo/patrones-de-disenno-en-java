package chiqui.ej05_decorator;

import java.util.List;

/**
 *
 * @author Chiqui Chexmo <thechexmo@gmail.com>
 */
public interface Vendible {

    public float getPrecioTotal();

    public List<String> getDescripciones();
}
