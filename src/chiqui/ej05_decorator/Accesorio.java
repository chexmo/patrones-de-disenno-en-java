package chiqui.ej05_decorator;

import chiqui.ej05_decorator.accesorios.AccesorioNulo;
import chiqui.ej05_decorator.accesorios.Bolas;
import chiqui.ej05_decorator.accesorios.Luces;
import java.util.HashMap;

/**
 *
 * @author Chiqui Chexmo <thechexmo@gmail.com>
 */
abstract public class Accesorio implements Vendible {

    public static final HashMap<String, Float> preciosAccesorios = new HashMap<>();

    static {
        preciosAccesorios.put("Bolas", 90.5f);
        preciosAccesorios.put("Luces", 120f);
        preciosAccesorios.put("Estrellas", 75.75f);
        preciosAccesorios.put("Monios", 120f);
    }

    protected float precio;
    protected Vendible contenido;

    public Accesorio(float precio, Vendible contenido) {
        this.precio = precio;
        this.contenido = contenido;
    }

    @Override
    public float getPrecioTotal() {
        return this.contenido.getPrecioTotal() + this.precio;
    }

    public Vendible getContenido() {
        return contenido;
    }

    public static Vendible crearAccesorioPara(String tipo, Vendible vendible) {
        switch (tipo) {
            case "Bolas":
                return new Bolas(preciosAccesorios.get(tipo), vendible);
            case "Luces":
                return new Luces(preciosAccesorios.get(tipo), vendible);
            case "Estrellas":
                return new Bolas(preciosAccesorios.get(tipo), vendible);
            case "Monios":
                return new Bolas(preciosAccesorios.get(tipo), vendible);
            default:
                System.out.println("Algo salio mal en la factoria");
                return new AccesorioNulo(0f, vendible);
        }
    }

}
