package chiqui.ej05_decorator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Chiqui Chexmo <thechexmo@gmail.com>
 */
public class Arbol implements Vendible {

    public static final HashMap<String, Float> variedadesDeArbol = new HashMap();

    static {
        variedadesDeArbol.put("Pequeño", 100f);
        variedadesDeArbol.put("Mediano", 200f);
        variedadesDeArbol.put("Grande", 400f);
    }

    private final String tamanno;

    public Arbol(String tamanno) {
        this.tamanno = tamanno;
    }

    @Override
    public float getPrecioTotal() {
        return variedadesDeArbol.get(tamanno);
    }

    @Override
    public List<String> getDescripciones() {
        ArrayList<String> descripciones = new ArrayList<String>();
        descripciones.add("Soy un arbol " + tamanno);
        return descripciones;
    }

}
