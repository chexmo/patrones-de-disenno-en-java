package chiqui.ej05_decorator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chiqui Chexmo <thechexmo@gmail.com>
 */
public class Ej05 {

    Vendible pedido;

    public Ej05() {
        pedido = new Arbol("Mediano");
    }

    public Vendible getPedido() {
        return pedido;
    }

    public void nuevoArbol(String tamanno) {
        this.pedido = new Arbol(tamanno);
    }

    public List<String> getTamannosArbol() {
        return new ArrayList<>(Arbol.variedadesDeArbol.keySet());
    }

    public void agregarAccesorio(String tipo) {
        this.pedido = Accesorio.crearAccesorioPara(tipo, pedido);
    }

    public List<String> getAccesorios() {
        return new ArrayList<>(Accesorio.preciosAccesorios.keySet());
    }

}
